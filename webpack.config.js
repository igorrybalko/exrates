//webpack.config.js
'use strict';

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry:{
        'polyfills': './src/ts/polyfills.ts',
        'app': "./src/ts/index.ts"
    },
    output: {
        path: path.resolve(__dirname, './js'),
        filename: "[name].js"
    },
    resolve: {
        // Add '.ts' and '.tsx' as a resolvable extension.
        extensions: [".ts", ".tsx", ".js", ".json", '.html']
    },
    //devtool: 'source-map',
    optimization: {
        // splitChunks: {
        //     chunks: "all"
        // },
        minimize: true
    },
    module: {
        rules:[   //загрузчик для ts
            {
                test: /\.ts$/, // определяем тип файлов
                use: [
                    {
                        loader: 'awesome-typescript-loader',
                        options: { configFileName: path.resolve(__dirname, 'tsconfig.json') }
                    } ,
                    'angular2-template-loader'
                ]
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.txt$/,
                use: 'raw-loader'
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new webpack.ContextReplacementPlugin(
            /angular(\\|\/)core/,
            path.resolve(__dirname, 'src'), // каталог с исходными файлами
            {} // карта маршрутов
        ),
        new UglifyJsPlugin({
             uglifyOptions:{
                compress: {
                    warnings: false
                },
                output: {
                    comments: false
                },
                sourceMap: true
            }
        })
    ]
};