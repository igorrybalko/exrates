import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule }   from '@angular/common/http';
import {Routes, RouterModule} from '@angular/router';

import { AppComponent }   from '../components/app.component';
import { BankexchangeComponent }   from '../components/pages/bankexchange.component';
// import { BlogComponent }   from '../components/blog.component';
// import { BlogItemComponent }   from '../components/blogItem.component';
import { NotFoundComponent }   from '../components/pages/not-found.component';

import { HeaderComponent }   from '../components/chunks/header.component';
import { FooterComponent }   from '../components/chunks/footer.component';
// import { PaginationComponent }   from '../components/pagination.component';
//import { HeaderMenuComponent }   from '../components/chunks/headerMenu.component';

// определение маршрутов
const appRoutes: Routes =[
    { path: '', component: BankexchangeComponent},
    // { path: 'about', component: AboutComponent},
    // { path: 'news', component: BlogComponent},
    // { path: 'news/:slug', component: BlogItemComponent},
    { path: '**', component: NotFoundComponent }
];


@NgModule({
    imports:      [ BrowserModule, FormsModule, HttpClientModule, RouterModule.forRoot(appRoutes) ],
    declarations: [
        AppComponent,
        NotFoundComponent,
        HeaderComponent,
        FooterComponent,
        BankexchangeComponent,
       // HeaderMenuComponent,
    ],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }