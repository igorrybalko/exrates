import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class HttpService{

    constructor(private http: HttpClient){ }

    getData(url: string){
        return this.http.get(url);
    }

    // postData(url: string, body: object){
    //     return this.http.post(url, body);
    // }
}