import { Component } from '@angular/core';


@Component({
    selector: 'header-chunk',
    templateUrl: '../../../tpl/chunks/headerChunk.html'
})
export class HeaderComponent {
    siteName: string = 'Site name';
}