import { Component} from '@angular/core';

@Component({
    selector: 'not-found-app',
    templateUrl: '../../../tpl/pages/errorPage.html'
})
export class NotFoundComponent { }